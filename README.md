# DEMO API REST
Demo project of an API REST using Spring Boot and Angular


## The server
Maven multimodule project with three independent layers (web, core and dao) and a fourth layer for DTOs, which is transversal to the others


## The client
SPA using Angular CLI which consist of two main components (app and country-form).
The project is located in a folder called client inside the web layer in order to keep the entire project simple
	
	
## Installation pre-requisites
1. Node.js (comes along with npm package manager which is also required)
2. Angular CLI
3. Maven
	
## Running project
1. Run **maven install** to get the executable jar
2. Execute **java -jar path_to_m2_repo/.m2/repository/net/ddns/pablosesteban/demo-api-rest-web/0.0.1-SNAPSHOT/demo-api-rest-web-0.0.1-SNAPSHOT.jar** to start the server
3. Go to URL **http://localhost:8080/demoapirest**
4. API documentation can be found at **http://localhost:8080/demoapirest/swagger-ui.html**
	
