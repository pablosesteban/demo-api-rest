/**
 * @author Pablo Santamarta Esteban <pablosesteban@gmail.com>
 */
package net.ddns.pablosesteban.core.api;

import java.util.List;

import net.ddns.pablosesteban.core.exception.DemoApiRestServiceException;
import net.ddns.pablosesteban.dto.input.CountryInput;
import net.ddns.pablosesteban.dto.response.CountryResponse;

public interface CountryServiceApi {
	CountryResponse getCountry(Integer countryId) throws DemoApiRestServiceException;
	
	List<CountryResponse> getAllCountries();
	
	CountryResponse addCountry(CountryInput countryInput) throws DemoApiRestServiceException;
	
	CountryResponse modifyCountry(CountryInput countryInput) throws DemoApiRestServiceException;
	
	CountryResponse deleteCountry(String name) throws DemoApiRestServiceException;
}
