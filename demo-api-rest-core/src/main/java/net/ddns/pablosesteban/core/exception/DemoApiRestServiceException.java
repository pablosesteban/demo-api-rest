/**
 * @author Pablo Santamarta Esteban <pablosesteban@gmail.com>
 */
package net.ddns.pablosesteban.core.exception;

public class DemoApiRestServiceException extends Exception {
	private static final long serialVersionUID = -7408680335399756591L;

	public DemoApiRestServiceException(String message) {
		super(message);
	}
}
