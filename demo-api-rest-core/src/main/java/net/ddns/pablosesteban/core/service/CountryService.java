/**
 * @author Pablo Santamarta Esteban <pablosesteban@gmail.com>
 */
package net.ddns.pablosesteban.core.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.ddns.pablosesteban.core.api.CountryServiceApi;
import net.ddns.pablosesteban.core.exception.DemoApiRestServiceException;
import net.ddns.pablosesteban.dao.api.CountryDaoApi;
import net.ddns.pablosesteban.dao.entity.Country;
import net.ddns.pablosesteban.dto.input.CountryInput;
import net.ddns.pablosesteban.dto.response.CountryResponse;

@Service
public class CountryService implements CountryServiceApi {
	@Autowired
	private CountryDaoApi countryDao;
	
	@Override
	public CountryResponse getCountry(Integer countryId) throws DemoApiRestServiceException {
		Optional<Country> countryEntityOptional = countryDao.findById(countryId);
		
		if (!countryEntityOptional.isPresent()) {
			throw new DemoApiRestServiceException("Country not found with id: " + countryId);
		}
		
		return new CountryResponse(countryEntityOptional.get().getId(), countryEntityOptional.get().getName(), String.valueOf(countryEntityOptional.get().getPopulation()));
	}

	@Override
	public List<CountryResponse> getAllCountries() {
		List<CountryResponse> countries = new ArrayList<>();
		
		List<Country> countryEntities = countryDao.findAll();
		
		for (Country countryEntity : countryEntities) {
			countries.add(new CountryResponse(countryEntity.getId(), countryEntity.getName(), String.valueOf(countryEntity.getPopulation())));
		}
		
		return countries;
	}

	@Override
	public CountryResponse addCountry(CountryInput countryInput) throws DemoApiRestServiceException {
		Country countryEntity = countryDao.findByName(countryInput.getName());
		
		if (countryEntity != null) {
			throw new DemoApiRestServiceException("Country already exists: " + countryEntity);
		}
		
		countryEntity = new Country(countryInput.getName(), Integer.parseInt(countryInput.getPopulation()));
		
		countryDao.save(countryEntity);
		
		countryEntity = countryDao.findByName(countryInput.getName());
		
		return new CountryResponse(countryEntity.getId(), countryEntity.getName(), String.valueOf(countryEntity.getPopulation()));
	}

	@Override
	public CountryResponse modifyCountry(CountryInput countryInput) throws DemoApiRestServiceException {
		Country countryEntity = countryDao.findByName(countryInput.getName());
		
		if (countryEntity == null) {
			throw new DemoApiRestServiceException("Country not found with name: " + countryInput.getName());
		}
		
		countryEntity.setPopulation(Integer.parseInt(countryInput.getPopulation()));
		
		countryDao.save(countryEntity);
		
		countryEntity = countryDao.getOne(countryEntity.getId());
		
		return new CountryResponse(countryEntity.getId(), countryEntity.getName(), String.valueOf(countryEntity.getPopulation()));
	}

	@Override
	public CountryResponse deleteCountry(String name) throws DemoApiRestServiceException {
		Country countryEntity = countryDao.findByName(name);
		
		if (countryEntity == null) {
			throw new DemoApiRestServiceException("Country not found with name: " + name);
		}
		
		countryDao.delete(countryEntity);
		
		return new CountryResponse(countryEntity.getId(), countryEntity.getName(), String.valueOf(countryEntity.getPopulation()));
	}
}
