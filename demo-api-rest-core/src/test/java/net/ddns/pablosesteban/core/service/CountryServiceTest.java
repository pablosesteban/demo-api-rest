/**
 * @author Pablo Santamarta Esteban <pablosesteban@gmail.com>
 */
package net.ddns.pablosesteban.core.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import net.ddns.pablosesteban.core.exception.DemoApiRestServiceException;
import net.ddns.pablosesteban.core.service.CountryService;
import net.ddns.pablosesteban.dao.api.CountryDaoApi;
import net.ddns.pablosesteban.dao.entity.Country;
import net.ddns.pablosesteban.dto.input.CountryInput;

@RunWith(MockitoJUnitRunner.class)
public class CountryServiceTest {
	@Mock
	private CountryDaoApi countryDaoMock;
	
	@InjectMocks
	private CountryService countryService;
	
	@Test
	public void testGetCountry() throws DemoApiRestServiceException {
		Country countryEntity = new Country("Spain", 12345);
		countryEntity.setId(1);
		
		Mockito.when(countryDaoMock.findById(Mockito.anyInt())).thenReturn(Optional.of(countryEntity));
		
		countryService.getCountry(1);
	}
	
	@Test(expected=DemoApiRestServiceException.class)
	public void testGetCountry_notFound() throws DemoApiRestServiceException {
		Country countryEntity = null;
		
		Mockito.when(countryDaoMock.findById(Mockito.anyInt())).thenReturn(Optional.ofNullable(countryEntity));
		
		countryService.getCountry(1);
	}
	
	@Test
	public void testGetAllCountries() {
		Country countryEntity = new Country("Spain", 12345);
		countryEntity.setId(1);
		
		List<Country> countryEntities = new ArrayList<>();
		countryEntities.add(countryEntity);
		
		Mockito.when(countryDaoMock.findAll()).thenReturn(countryEntities);
		
		countryService.getAllCountries();
	}
	
	@Test
	public void testAddCountry() throws DemoApiRestServiceException {
		Country countryEntity = new Country("Spain", 12345);
		countryEntity.setId(1);
		
		Mockito.when(countryDaoMock.findByName(Mockito.anyString())).thenAnswer(new Answer<Country>() {
			private int numberOfInvocations;
			
			@Override
			public Country answer(InvocationOnMock iom) throws Throwable {
				if (numberOfInvocations++ > 0) {
					return countryEntity;
				}
				
				return null;
			}
		});

		CountryInput countryInput = new CountryInput("Spain", "12345");
		
		countryService.addCountry(countryInput);
	}
	
	@Test(expected=DemoApiRestServiceException.class)
	public void testAddCountry_alreadyExists() throws DemoApiRestServiceException {
		Country countryEntity = new Country("Spain", 12345);
		countryEntity.setId(1);
		
		Mockito.when(countryDaoMock.findByName(Mockito.anyString())).thenReturn(countryEntity);

		CountryInput countryInput = new CountryInput("Spain", "12345");
		
		countryService.addCountry(countryInput);
	}
	
	@Test
	public void testModifyCountry() throws DemoApiRestServiceException {
		Country countryEntity = new Country("Spain", 12345);
		countryEntity.setId(1);
		
		Mockito.when(countryDaoMock.findByName(Mockito.anyString())).thenReturn(countryEntity);
		Mockito.when(countryDaoMock.getOne(Mockito.anyInt())).thenReturn(countryEntity);
		
		CountryInput countryInput = new CountryInput("Spain", "67890");
		
		countryService.modifyCountry(countryInput);
	}
	
	@Test(expected=DemoApiRestServiceException.class)
	public void testModifyCountry_notFound() throws DemoApiRestServiceException {
		Country countryEntity = null;
		
		Mockito.when(countryDaoMock.findByName(Mockito.anyString())).thenReturn(countryEntity);
		
		CountryInput countryInput = new CountryInput("Spain", "12345");
		
		countryService.modifyCountry(countryInput);
	}
	
	@Test
	public void deleteCountry() throws DemoApiRestServiceException {
		Country countryEntity = new Country("Spain", 12345);
		countryEntity.setId(1);
		
		Mockito.when(countryDaoMock.findByName(Mockito.anyString())).thenReturn(countryEntity);
		
		countryService.deleteCountry("Spain");
	}
	
	@Test(expected=DemoApiRestServiceException.class)
	public void deleteCountry_alreadyExists() throws DemoApiRestServiceException {
		Country countryEntity = null;
		
		Mockito.when(countryDaoMock.findByName(Mockito.anyString())).thenReturn(countryEntity);
		
		countryService.deleteCountry("Spain");
	}
}
