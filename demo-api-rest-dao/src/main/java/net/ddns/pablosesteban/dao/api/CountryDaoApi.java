/**
 * @author Pablo Santamarta Esteban <pablosesteban@gmail.com>
 */
package net.ddns.pablosesteban.dao.api;

import org.springframework.data.jpa.repository.JpaRepository;

import net.ddns.pablosesteban.dao.entity.Country;

public interface CountryDaoApi extends JpaRepository<Country, Integer> {
	Country findByName(String name);
}
