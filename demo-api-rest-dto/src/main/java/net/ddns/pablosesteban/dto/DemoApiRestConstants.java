/**
 * @author Pablo Santamarta Esteban <pablosesteban@gmail.com>
 */
package net.ddns.pablosesteban.dto;

public final class DemoApiRestConstants {
	private DemoApiRestConstants() {}
	
	public static final String REGEX_NUMBER = "[0-9]+";
	public static final String REGEX_LETTER = "[a-zA-Z]+";
	
	public static final String DESCRIPTION_NUMBER = "Must be a number";
	public static final String DESCRIPTION_LETTER = "Must be letters";
	public static final String DESCRIPTION_REQUIRED = "Data required";
}
