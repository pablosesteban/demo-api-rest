/**
 * @author Pablo Santamarta Esteban <pablosesteban@gmail.com>
 */
package net.ddns.pablosesteban.dto.input;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import net.ddns.pablosesteban.dto.DemoApiRestConstants;
import net.ddns.pablosesteban.dto.input.constraint.AddGroup;
import net.ddns.pablosesteban.dto.input.constraint.DeleteGroup;
import net.ddns.pablosesteban.dto.input.constraint.ModifyGroup;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class CountryInput {
	@ApiParam(name = "name", value = "Country name")
	@NotNull(message=DemoApiRestConstants.DESCRIPTION_REQUIRED, groups= {AddGroup.class, ModifyGroup.class, DeleteGroup.class})
	@Pattern(regexp=DemoApiRestConstants.REGEX_LETTER, message=DemoApiRestConstants.DESCRIPTION_LETTER, groups= {AddGroup.class, ModifyGroup.class, DeleteGroup.class})
	private String name;

	@ApiParam(name = "population", value = "Country population")
	@NotNull(message=DemoApiRestConstants.DESCRIPTION_REQUIRED, groups= {AddGroup.class, ModifyGroup.class})
	@Pattern(regexp=DemoApiRestConstants.REGEX_NUMBER, message=DemoApiRestConstants.DESCRIPTION_NUMBER, groups= {AddGroup.class, ModifyGroup.class})
	private String population;
}
