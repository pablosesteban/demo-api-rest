import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CountryFormComponent } from './country-form/country-form.component';

const routes: Routes = [
    { path: 'addCountry', component: CountryFormComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
