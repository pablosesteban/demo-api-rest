import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { Country } from './model/country';
import { CountryService } from './service/country.service';
import { CountryInput } from './model/country-input';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
    title: string;
    countries: Country[];
    countryInput: CountryInput;
    
    constructor(private route: ActivatedRoute, private router: Router, private countryService: CountryService) {
        this.title = "Demo Api Rest";
    }
    
    ngOnInit() {
        this.countryService.findAll().subscribe(data => {
            this.countries = data;
        });
    }
    
    clickEdit(name: string, population: string) {
        let navigationExtras: NavigationExtras = {
            queryParams: {
                "name": name,
                "population": population,
                "isEdit": true
            }
        };
        
        this.router.navigate(['/addCountry'], navigationExtras);
    }
    
    clickDelete(i: number) {
        this.countryService.delete(this.countries[i].name).subscribe(
            result => this.countryService.findAll().subscribe(
                data => this.countries = data
            )
        );
        
        let navigationExtras: NavigationExtras = {
            queryParams: {
                "name": "",
                "population": "",
                "isEdit": false
            }
        };
        
        this.router.navigate(['/addCountry'], navigationExtras);
    }
}
