import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { CountryInput } from '../model/country-input';
import { CountryService } from '../service/country.service';
import { AppComponent } from '../app.component';

@Component({
  selector: 'app-country-form',
  templateUrl: './country-form.component.html',
  styleUrls: ['./country-form.component.css']
})
export class CountryFormComponent implements OnInit {
    countryInput: CountryInput;
    isEdit: boolean;
    
    constructor(private route: ActivatedRoute, private router: Router, private countryService: CountryService, private appComponent: AppComponent) {
        this.countryInput = new CountryInput();
    }

    ngOnInit() {
        this.route.queryParams.subscribe(params => {
            this.countryInput.name = params['name'];
            this.countryInput.population = params['population'];
            this.isEdit = params['isEdit'];
        });
    }
    
    onSubmit() {
        if (this.isEdit) {
            this.countryService.modify(this.countryInput).subscribe(
                result => this.countryService.findAll().subscribe(
                    data => this.appComponent.countries = data
                )
            );
        }else {
            this.countryService.save(this.countryInput).subscribe(
                result => this.countryService.findAll().subscribe(
                    data => this.appComponent.countries = data
                )
            );
        }
        
        this.isEdit = false;
        
        let navigationExtras: NavigationExtras = {
            queryParams: {
                "name": "",
                "population": "",
                "isEdit": false
            }
        };
        
        this.router.navigate(['/addCountry'], navigationExtras);
    }
}
