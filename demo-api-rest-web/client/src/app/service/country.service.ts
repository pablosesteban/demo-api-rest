import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Country } from '../model/country';
import { CountryInput } from '../model/country-input';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class CountryService {
    private countryUrl: string;
    
    constructor(private http: HttpClient) {
        this.countryUrl = 'http://localhost:8080/demoapirest/countries';
    }
    
    public findAll(): Observable<Country[]> {
        return this.http.get<Country[]>(this.countryUrl);
    }
    
    public save(countryInput: CountryInput) {
        return this.http.post<CountryInput>(this.countryUrl, countryInput);
    }
    
    public modify(countryInput: CountryInput) {
        return this.http.put<CountryInput>(this.countryUrl, countryInput);
    }
    
    public delete(name: string) {
        return this.http.delete<string>(`${this.countryUrl}/${name}`);
    }
}
