/**
 * @author Pablo Santamarta Esteban <pablosesteban@gmail.com>
 */
package net.ddns.pablosesteban.web.controller;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import net.ddns.pablosesteban.core.api.CountryServiceApi;
import net.ddns.pablosesteban.core.exception.DemoApiRestServiceException;
import net.ddns.pablosesteban.dto.DemoApiRestConstants;
import net.ddns.pablosesteban.dto.input.CountryInput;
import net.ddns.pablosesteban.dto.input.constraint.AddGroup;
import net.ddns.pablosesteban.dto.input.constraint.ModifyGroup;
import net.ddns.pablosesteban.dto.response.CountryResponse;

@Api(value="Country API", tags="Country")
@CrossOrigin(origins="http://localhost:4200")
@RestController
@RequestMapping(path="/countries")
@Validated
public class CountryController {
	private static final Logger LOG = LoggerFactory.getLogger(CountryController.class);
	
	@Autowired
	private CountryServiceApi countryService;
	
	@ApiOperation(value="Get all countries")
	@GetMapping
	public ResponseEntity<List<CountryResponse>> getAllCountries() {
		List<CountryResponse> countries = countryService.getAllCountries();
		
		return ResponseEntity.ok().body(countries);
	}
	
	@ApiOperation(value="Get country")
	@GetMapping(path="/{id}")
	public ResponseEntity<CountryResponse> getCountry(
			@ApiParam(name = "id", value = "Country id")
			@PathVariable
			@Pattern(regexp=DemoApiRestConstants.REGEX_NUMBER, message=DemoApiRestConstants.DESCRIPTION_NUMBER)
			String id
	) throws DemoApiRestServiceException {
		CountryResponse country = countryService.getCountry(Integer.parseInt(id));
		
		return ResponseEntity.ok().body(country);
	}
	
	@ApiOperation(value="Add country")
	@PostMapping
	public ResponseEntity<CountryResponse> addCountry(@RequestBody @Validated(AddGroup.class) @Valid CountryInput countryInput) throws DemoApiRestServiceException {
		CountryResponse country = countryService.addCountry(countryInput);
		
		LOG.info("Country added: {}", country);
		
		return ResponseEntity.ok().body(country);
	}
	
	@ApiOperation(value="Modify country")
	@PutMapping
	public ResponseEntity<CountryResponse> modifyCountry(@RequestBody @Validated(ModifyGroup.class) @Valid CountryInput countryInput) throws DemoApiRestServiceException {
		CountryResponse country = countryService.modifyCountry(countryInput);
		
		LOG.info("Country modified: {}", country);
		
		return ResponseEntity.ok().body(country);
	}
	
	@ApiOperation(value="Delete country")
	@DeleteMapping(path="{name}")
	public ResponseEntity<CountryResponse> deleteCountry(
			@ApiParam(name = "name", value = "Country name")
			@PathVariable
			@Pattern(regexp=DemoApiRestConstants.REGEX_LETTER, message=DemoApiRestConstants.DESCRIPTION_LETTER)
			String name
	) throws DemoApiRestServiceException {
		CountryResponse country = countryService.deleteCountry(name);
		
		LOG.info("Country deleted: {}", country);
		
		return ResponseEntity.ok().body(country);
	}
}
