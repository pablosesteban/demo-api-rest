/**
 * @author Pablo Santamarta Esteban <pablosesteban@gmail.com>
 */
package net.ddns.pablosesteban.web.exception;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import net.ddns.pablosesteban.core.exception.DemoApiRestServiceException;
import net.ddns.pablosesteban.dto.response.ApiErrorResponse;

@RestControllerAdvice
public class DemoApiRestExceptionHandler {
	private static final Logger LOG = LoggerFactory.getLogger(DemoApiRestExceptionHandler.class);
	
	@ExceptionHandler(DemoApiRestServiceException.class)
	public ResponseEntity<ApiErrorResponse> handleClientError(DemoApiRestServiceException darse) {
		ApiErrorResponse apiErrorDto = createApiErrorDto(HttpStatus.BAD_REQUEST, null, darse.getMessage());
		
		LOG.error(darse.getMessage(), darse);
		
		return new ResponseEntity<ApiErrorResponse>(apiErrorDto, HttpStatus.BAD_REQUEST);
	}
	
	@ExceptionHandler(ConstraintViolationException.class)
	public ResponseEntity<ApiErrorResponse> handleClientError(ConstraintViolationException cve) {
		ApiErrorResponse apiErrorDto = null;
		
		for (ConstraintViolation<?> cv : cve.getConstraintViolations()) {
			apiErrorDto = createApiErrorDto(HttpStatus.BAD_REQUEST, cv.getInvalidValue().toString(), cv.getMessageTemplate());
		}
		
		LOG.error(cve.getMessage(), cve);
		
		return new ResponseEntity<ApiErrorResponse>(apiErrorDto, HttpStatus.BAD_REQUEST);
	}
	
	@ExceptionHandler(MethodArgumentNotValidException.class)
	public ResponseEntity<ApiErrorResponse> handleClientError(MethodArgumentNotValidException manve) {
		BindingResult bindingResult = manve.getBindingResult();
		
		FieldError fieldError = bindingResult.getFieldError();
		
		ApiErrorResponse apiErrorDto = createApiErrorDto(HttpStatus.BAD_REQUEST, fieldError.getField(), fieldError.getDefaultMessage());
		
		LOG.error(manve.getMessage(), manve);
		
		return new ResponseEntity<ApiErrorResponse>(apiErrorDto, HttpStatus.BAD_REQUEST);
	}
	
	@ExceptionHandler(Throwable.class)
	public ResponseEntity<ApiErrorResponse> handleApiError(Throwable t) {
		ApiErrorResponse apiErrorDto = createApiErrorDto(HttpStatus.INTERNAL_SERVER_ERROR, null, "Something went wrong. Please contact pablosesteban@gmail.com");
		
		LOG.error(t.getMessage(), t);
		
		return new ResponseEntity<ApiErrorResponse>(apiErrorDto, HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	private ApiErrorResponse createApiErrorDto(HttpStatus status, String parameter, String message) {
		ApiErrorResponse apiErrorDto = new ApiErrorResponse();
		
		apiErrorDto.setStatus(status);
		apiErrorDto.setParameter(parameter);
		apiErrorDto.setMessage(message);
		
		return apiErrorDto;
	}
}
