/**
 * @author Pablo Santamarta Esteban <pablosesteban@gmail.com>
 */
package net.ddns.pablosesteban.web.exception;

import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.bind.MethodArgumentNotValidException;

import net.ddns.pablosesteban.dto.input.CountryInput;
import net.ddns.pablosesteban.dto.response.ApiErrorResponse;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment=WebEnvironment.RANDOM_PORT)
public class DemoApiRestExceptionHandlerTest {
	@Autowired
	private TestRestTemplate testRestTemplate;
	
	private String baseUrl = "/countries";
	
	@Test
	public void testHandleClientErrorConstraintViolationException() {
		ResponseEntity<ApiErrorResponse> responseEntity = testRestTemplate.exchange(baseUrl + "/{id}", HttpMethod.GET, new HttpEntity<>(null), ApiErrorResponse.class, "abcd");
		
		Assertions.assertThat(responseEntity.getBody().getStatus()).isEqualTo(HttpStatus.BAD_REQUEST);
		Assertions.assertThat(responseEntity.getBody().getParameter()).isEqualTo("abcd");
		Assertions.assertThat(responseEntity.getBody().getMessage()).isEqualTo("Must be a number");
	}
	
	@Test
	public void testHandleClientErrorMethodArgumentNotValidException() {
		CountryInput countryInput = new CountryInput();
		countryInput.setName("Spain");
		
		ResponseEntity<ApiErrorResponse> responseEntity = testRestTemplate.exchange(baseUrl, HttpMethod.POST, new HttpEntity<>(countryInput), ApiErrorResponse.class);
		
		Assertions.assertThat(responseEntity.getBody().getStatus()).isEqualTo(HttpStatus.BAD_REQUEST);
		Assertions.assertThat(responseEntity.getBody().getParameter()).isEqualTo("population");
		Assertions.assertThat(responseEntity.getBody().getMessage()).isEqualTo("Data required");
	}
	
	@Test
	public void testHandleClientErrorDemoApiRestExceptionHandler() {
		CountryInput countryInput = new CountryInput();
		countryInput.setName("12345");
		countryInput.setPopulation("12345");
		
		ResponseEntity<ApiErrorResponse> responseEntity = testRestTemplate.exchange(baseUrl, HttpMethod.POST, new HttpEntity<>(countryInput), ApiErrorResponse.class);
		
		Assertions.assertThat(responseEntity.getBody().getStatus()).isEqualTo(HttpStatus.BAD_REQUEST);
		Assertions.assertThat(responseEntity.getBody().getParameter()).isEqualTo("name");
		Assertions.assertThat(responseEntity.getBody().getMessage()).isEqualTo("Must be letters");
	}
}
